<?php

namespace App\Http\Controllers;

use App\Models\tictactoe;
use Illuminate\Http\Request;
use DB;

class TictactoeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public $user="";
    
    public function createboardgame(){
        $html="<tr>";
        $tr=1;
        $position=0;
        for ($i=1; $i < 10; $i++) { 
            if($tr<3){
                $html.="<td id='block{$position}'> <center><button class='btn btn-lg btn-warning' id='button{$position}'"." onclick=game('block{$position}');".">???</button></center> </td>";
                $tr++;
            }else{
                $html.="<td id='block{$position}'> <center><button class='btn btn-lg btn-warning' id='button{$position}'"." onclick=game('block{$position}');".">???</button></center> </td></tr>";
                $tr=1;
            }
            $position++;
        }
        return $html;
    }

   

    public function index()
    {   
      
        $board=self::createboardgame();
        $data = tictactoe::latest('id')->first();   
        $player="";
        if($data->userplayer2!=''){
            $player=$data->userplayer2;
        }else{
            $player=$data->userplayer1;
        }
        return view("gameTictactoe.boardgame")->with("board",$board)
                                              ->with("nameUser", $player)
                                              ->with('player1',$data->userplayer1)
                                              ->with('player2',$data->userplayer2);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('gameTictactoe.create');
    }
    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $name=isset($request['name']) ? $request['name']:null;
        $searchgame=DB::select('SELECT * FROM tictactoes WHERE userplayer2="" ;');
        $tictactoegamenew= new tictactoe;
        if(!empty($searchgame)){
            $findgame=tictactoe::findOrFail($searchgame[0]->id);
            $findgame->userplayer1=$searchgame[0]->userplayer1;
            $findgame->userplayer2=$name;
            $findgame->save();
            
        }else{
            $tictactoegamenew->userplayer1=$name;
            $tictactoegamenew->userplayer2="";
            $tictactoegamenew->save();
        }

        return view("gameTictactoe.index");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\tictactoe  $tictactoe
     * @return \Illuminate\Http\Response
     */
    public function show(tictactoe $tictactoe)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\tictactoe  $tictactoe
     * @return \Illuminate\Http\Response
     */
    public function edit(tictactoe $tictactoe,Request $request)
    {
        var_dump($request);
    }

    public function updateBoard(Request $request)
    {
        var_dump($request);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\tictactoe  $tictactoe
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, tictactoe $tictactoe)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\tictactoe  $tictactoe
     * @return \Illuminate\Http\Response
     */
    public function destroy(tictactoe $tictactoe)
    {
        //
    }
}
