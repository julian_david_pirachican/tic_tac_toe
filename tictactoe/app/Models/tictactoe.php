<?php

namespace App\Models;
use DB;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class tictactoe extends Model
{
    use HasFactory;
    protected $fillable = [
        'id'
       ,'userplayer1'
       ,'userplayer2'
    ];
    // public function showinfo(){
    //     $data=array();
    //     $data['info']= DB::select('SELECT * FROM tictactoes;');
    //     return $data;
    // }
}
