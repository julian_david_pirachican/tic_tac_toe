<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>GameTicTacToe</title>
    <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
    <script src="{{ asset('js/app.js') }}" defer></script>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    
    
    <script src='{{asset("js/board.js")}}'></script>
</head>
<body>

<div class="container">
    <div class="card" style="margin-top:10%;">
        <div class="card-content">
            <center>
                <h4>Bienvenido a Tic-tac-toe {{$nameUser}}</h4>
            </center>
            <p id="msgturn"></p>
            <table  class="table table-striped">       
                {!!$board!!}
            </table>
            <center> <h5> Jugador 1: {{$player1}} @if($player2!='') vs Jugador 2:{{$player2}}  @endif </h5></center>
            <div class="row">
                <div class="col-md-10">
                    <a href="{{url('/gameTictactoe/create')}}" class="btn btn-primary">Nuevo Jugador</a>
                </div> 
                <div class="col-md-2">
                    <button class="btn btn-danger" type="reset" id="resetgame" onclick="resetgame();">Reiniciar Partida</button>
                </div>
            </div>
        </div>
    </div>

</div>

</body>
</html>