<script src="{{ asset('js/app.js') }}" ></script>
   <link href="{{ asset('css/app.css') }}" rel="stylesheet">
<div class="container">
    <div class="card" style="margin-top=10%;">
        <div class="card-content">
            <div class="row" >
                <div class="col-md-12">
                    <form action="{{ url('/gameTictactoe') }}" method="post">
                     @csrf
                     @include("gameTictactoe.form")
                    </form>
                </div>
            </div>
        </div>
   </div>
</div>