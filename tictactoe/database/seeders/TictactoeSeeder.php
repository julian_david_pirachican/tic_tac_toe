<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\tictactoe;

class TictactoeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tictactoenewgame= new tictactoe;
        $tictactoenewgame->userplayer1="Usern1";
        $tictactoenewgame->userplayer2="Usern2";
        $tictactoenewgame->save();
    }
}
