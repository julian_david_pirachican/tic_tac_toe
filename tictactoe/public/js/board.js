var turn="X";
var  WINNINGS = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6]
  ];
 var GAME_STATE = ["", "", "", "", "", "", "", "",""];
 let turnnumber=0;
console.log( $('meta[name="csrf-token"]').attr('content'));

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')     
    }
}); 
function game(tdclick){
   
    let replacecontent=document.getElementById(tdclick);
    let player1= "x";
    let player2="O";
    let message="";
    let idplace=replacecontent.children[0].children[0].id;

    let numplace=idplace.substring(idplace.length - 1,idplace.length );
    let playingnow=""
    if(turn===player1){
        turn=player2;
        playingnow="player 1";
        document.getElementById(replacecontent.children[0].children[0].id).innerHTML=player1
        document.getElementById(replacecontent.children[0].children[0].id).disabled=true
        message=document.getElementById("msgturn");
        message.innerHTML="Es el turno de "+ playingnow;
        GAME_STATE[numplace]=player1;
     
        turnnumber+=1;
    }else{
        turn=player1;
        playingnow="player 2";
        document.getElementById(replacecontent.children[0].children[0].id).innerHTML=player2
        document.getElementById(replacecontent.children[0].children[0].id).disabled=true
        message=document.getElementById("msgturn");
        message.innerHTML="Es el turno de player "+ playingnow;
        GAME_STATE[numplace]=player2;
        turnnumber+=1;
    }

    // 

    validategame();
    let publishwinner=validategame();
    if(publishwinner){
        alert("Gano" +playingnow);
        for (let i = 0; i < 9; i++) {
            document.getElementById("button"+i).disabled=true
        }
        
    }
    if(turnnumber==9){
        if(!publishwinner){
            alert("Empate");
        }
    }
}

function resetgame(){
    location.reload();
}


function validategame(){
    var winner=false;
    for (let index = 0; index < WINNINGS.length; index++) {
        const winresult = WINNINGS[index];
        let position1 = GAME_STATE[winresult[0]]
           ,position2 = GAME_STATE[winresult[1]]
           ,position3 = GAME_STATE[winresult[2]] ;
        
        if (position1 === '' || position2 === '' || position3 === '') {
            continue;
        }

        if (position1 === position2 && position2 === position3) {
            winner = true ;
            break;
        }
    }
    let stategame=false;
    if (winner) {
       stategame=true;
        console.log("ganador");
    }else{
        stategame=false;
        console.log("empate");

    }

    return stategame;
}